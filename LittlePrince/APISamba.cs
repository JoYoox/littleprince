﻿using System;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using System.Net.Http.Headers;

namespace LittlePrince
{
    public class APISamba
    {
        private const string URL = "http://api.net-a-porter.com/MRP/";
        private static string urlParameters;
        public string error_code { get; private set; }

        public string GetValueFromAPI(string PID, string country)
        {
            error_code = null;
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(String.Concat(URL, country, "/en/detail/", PID));

                // Add an Accept header for JSON format.
                client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));

                // List data response.
                HttpResponseMessage response = client.GetAsync(urlParameters).Result;  // Blocking call! Program will wait here until a response is received or a timeout occurs.
                if (response.IsSuccessStatusCode)
                {
                    var content = response.Content.ReadAsStringAsync().Result;
                    return content;
                }
                else
                {
                    error_code = response.StatusCode.ToString();
                    return null;
                }

            }
        }


    }

}


