﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LittlePrince
{
    class VariantConverter
    {

        public IDictionary<string[], OnlinePrice> dic_variantcountry_onlineprice = new Dictionary<string[], OnlinePrice>();
        private LittlePrinceForm _lpForm;
        public bool MappingFound;

        public void UpdateKey(IDictionary<string[],OnlinePrice> pids_dic,string error_filename, List<string[]> mappingVariant_PID)
        {

            _lpForm = new LittlePrinceForm();

            foreach (var i_key in pids_dic)
            {
                string pid_FromDictionary = i_key.Key[0];
                MappingFound = false;

                for(int i = 0; i < mappingVariant_PID.Count;i++)
                {
                    if(mappingVariant_PID[i][1].Equals(pid_FromDictionary))
                    {
                        dic_variantcountry_onlineprice.Add(new[] { mappingVariant_PID[i][0], i_key.Key[1] }, i_key.Value);
                        MappingFound = true;
                    }
                }

                if(MappingFound == false)
                {
                    _lpForm.WriteLine(String.Concat(pid_FromDictionary, ",VARIANT_MAPPING_NOTFOUND,", DateTime.Now.ToString("yyyyMMdd h:mm tt")), error_filename);                    
                }

            }

        }

    }
}
