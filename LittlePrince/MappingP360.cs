﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace LittlePrince
{
    class MappingP360
    {

        public List<string[]> mappingVariant_PID = new List<string[]>();

        public void DownloadMapping (string filename)
        {
            StreamReader streamReader = new StreamReader(filename);
            var regex = new Regex("[,;]");

            while (!streamReader.EndOfStream)
            {
                mappingVariant_PID.Add(regex.Split(streamReader.ReadLine()));
            }
        }

    }
}
