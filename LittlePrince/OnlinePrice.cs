﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using Newtonsoft.Json;

namespace LittlePrince
{
    public class Price
    {
        public string discountPercent { get; set; }
        public string divisor { get; set; }
        public string originalAmount { get; set; }
        public string amount { get; set; }
        public string currency { get; set; }
    }

    public class OnlinePrice
    {
        public Price price;
        public bool onSale { get; set; }
    }

    public class APIPriceDeserializer
    {
        public OnlinePrice API_ShortResult(string apiResult)
        {
            return JsonConvert.DeserializeObject<OnlinePrice>(apiResult);
        }
    }

}
