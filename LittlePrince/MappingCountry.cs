﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace LittlePrince
{
    class MappingCountry
    {
        public List<string[]> mappingCountry_Name = new List<string[]>();

        public void DownloadMapping(string filename)
        {
            StreamReader streamReader = new StreamReader(filename);
            var regex = new Regex("[,;]");

            while (!streamReader.EndOfStream)
            {
                mappingCountry_Name.Add(regex.Split(streamReader.ReadLine()));
            }
        }
    }
}
