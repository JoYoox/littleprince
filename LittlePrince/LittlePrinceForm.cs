﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LittlePrince
{
    public partial class LittlePrinceForm : Form
    {
        private FileReadingGateway _fileReadingGateway;
        private APISamba _apiSamba;
        private APIPriceDeserializer _apiPrice;
        private MatrixPID_OnlinePrice _matrixPIDPrice;
        private MappingP360 _mappingP360;
        private VariantConverter _variantConverter;
        private MappingCountry _mappingCountry;
        private CountryConverter _countryConverter;
        private PrinceTemplate _princeTemplate;

        public LittlePrinceForm()
        {
            InitializeComponent();
            _fileReadingGateway = new FileReadingGateway();
            _apiSamba = new APISamba();
            _apiPrice = new APIPriceDeserializer();
            _matrixPIDPrice = new MatrixPID_OnlinePrice();
            _variantConverter = new VariantConverter();
            _mappingP360 = new MappingP360();
            _mappingCountry = new MappingCountry();
            _countryConverter = new CountryConverter();
            _princeTemplate = new PrinceTemplate();
        }

        public string Number_PIDs
        {
            get
            { return this.text_PIDs.Text; }
            set
            { this.text_PIDs.Text = value; }
        }

        public string Error_Message
        {
            get
            { return this.text_error.Text; }
            set
            { this.text_error.Text = value; }
        }

        public void btn_execute_Click(object sender, EventArgs e)
        {

            //Check if the folder and the PIDs count is > 0
            if(this.text_folder.Text.Equals("") || this.text_PIDs.Text.Equals("0") )
            {
                this.text_error.Text = "Please refresh the folder and PID csv collection";
                return;
            }

            this.text_steps.Text = "Calling the PIDs from the API and creating a Matrix";

            this.ProgressBar.Minimum = 0;
            this.ProgressBar.Maximum = Convert.ToInt32(Math.Round((Convert.ToDouble(this.text_PIDs.Text) * 100) /75,0));
            this.ProgressBar.Step = 1;

            string filename = Path.Combine(this.text_folder.Text, "Log Files", "JSON Results", $"{ DateTime.Now.ToString("yyyyMMdd h_mm tt")}.dat");
            string error_filename = Path.Combine(this.text_folder.Text, "Log Files", "Error Logs", $"{ DateTime.Now.ToString("yyyyMMdd h_mm tt")}_errors.csv");

            for (int i_pids = 0;i_pids < _fileReadingGateway.PIDs_Dictionary.GetLength(0);i_pids ++)
            {

                string APIResult = _apiSamba.GetValueFromAPI(_fileReadingGateway.PIDs_Dictionary[i_pids,0], _fileReadingGateway.PIDs_Dictionary[i_pids, 1]);
                if(APIResult == null) {
                    WriteLine(String.Concat(_fileReadingGateway.PIDs_Dictionary[i_pids, 0], ",", _fileReadingGateway.PIDs_Dictionary[i_pids, 1], ",APICALL_FAILED,",_apiSamba.error_code,",", DateTime.Now.ToString("h:mm tt")), error_filename);
                    this.text_error.Text = String.Concat(_fileReadingGateway.PIDs_Dictionary[i_pids, 0], ",", _fileReadingGateway.PIDs_Dictionary[i_pids, 1], ",APICALL_FAILED,", _apiSamba.error_code,",", DateTime.Now.ToString("h:mm tt"));  }
                else
                {
                    WriteLine(APIResult, filename);
                    var onlinePrice = _apiPrice.API_ShortResult(APIResult);
                    _matrixPIDPrice.ConstructMatrix(String.Concat(_fileReadingGateway.PIDs_Dictionary[i_pids, 0], ",", _fileReadingGateway.PIDs_Dictionary[i_pids, 1]),onlinePrice);
                }

                ProgressBar_PerformStep();
                Thread.Sleep(50);
            }

            this.text_steps.Text = "Matrix was created. Converting the PIDs to VariantIDs and countries names";

            string mappingVariant_filename = Path.Combine(this.text_folder.Text, "Input Files" ,"MappingVariant.csv");
            _mappingP360.DownloadMapping(mappingVariant_filename);
            _variantConverter.UpdateKey(_matrixPIDPrice.dic_pidcountry_onlineprice,error_filename,_mappingP360.mappingVariant_PID);

            Thread.Sleep(100);

            this.text_steps.Text = String.Concat("Mapping was done! ", this.text_PIDs.Text, " PIDs mapped to ", _variantConverter.dic_variantcountry_onlineprice.Count, " Variants");

            string mappingCountryCode_filename = Path.Combine(this.text_folder.Text, "Input Files", "MappingCountryCode.csv");
            _mappingCountry.DownloadMapping(mappingCountryCode_filename);
            _countryConverter.UpdateKey(_variantConverter.dic_variantcountry_onlineprice, error_filename,_mappingCountry.mappingCountry_Name);

            Thread.Sleep(100);

            this.text_steps.Text = String.Concat("Country codes and standard currencies were added.");

            string princefullprice_template = Path.Combine(this.text_folder.Text, "CrossFiles", "FullPriceTemplate.xlsx");
            string princefullprice_ready = Path.Combine(this.text_folder.Text, "Output Files" , $"{ DateTime.Today.ToString("yyyyMMdd h_mm tt")}_FullPrice_Prince.xlsx");
            string exchangerate_filename = Path.Combine(this.text_folder.Text, "CrossFiles", "ExchangeRates.xlsx");

            //this.ProgressBar.Value = 0;
            //this.ProgressBar.Minimum = 0;
            //this.ProgressBar.Maximum = _countryConverter.dic_final_onlineprice.Count;
            //this.ProgressBar.Step = 1;
            //this.ProgressBar_Percentage.Text = "0%";

            this.text_steps.Text = "Filling Prince FullPriceTemplate";
            _princeTemplate.FullPrice_Template(_countryConverter.dic_final_onlineprice, princefullprice_template,princefullprice_ready,exchangerate_filename);
            this.ProgressBar.Value = Convert.ToInt32(Math.Round(this.ProgressBar.Maximum * 0.85, 0));
            this.ProgressBar_Percentage.Text = "85%";

            Thread.Sleep(100);

            string princemarkdown_template = Path.Combine(this.text_folder.Text, "CrossFiles", "MarkDownTemplate.xlsx");
            string princemarkdown_ready = Path.Combine(this.text_folder.Text, "Output Files", $"{ DateTime.Today.ToString("yyyyMMdd h_mm tt")}_MarkDown_Prince.xlsx");

            this.text_steps.Text = "Filling Prince MarkDownTemplate";
            _princeTemplate.MarkDown_Template(_countryConverter.dic_final_onlineprice, princemarkdown_template, princemarkdown_ready);

            this.text_steps.Text = "My work is done, please check the files in the folder";
            this.ProgressBar.Value = this.ProgressBar.Maximum;
            this.ProgressBar_Percentage.Text = "100%";

        }

        public void WriteLine(string line, string fileName)
        {
            var sc = new StreamWriter(fileName, true);
            sc.WriteLine(line);
            sc.Close();
        }

        private void picture_folder_Click(object sender, EventArgs e)
        {
            using (var fbd = new FolderBrowserDialog())
            {
                DialogResult dresult = fbd.ShowDialog();

                if (dresult == DialogResult.OK && !string.IsNullOrWhiteSpace(Path.Combine(fbd.SelectedPath, "Input Files")))
                {
                    string[] files = Directory.GetFiles(Path.Combine(fbd.SelectedPath, "Input Files"));
                    for (int i_files = 0; i_files < files.Length; i_files++)
                    {
                        if (files[i_files].Equals(Path.Combine(fbd.SelectedPath,"Input Files","PIDs.csv")))
                        {
                            if (_fileReadingGateway.Read_InputPIDs(Path.Combine(fbd.SelectedPath, "Input Files", "PIDs.csv"),Path.Combine(fbd.SelectedPath, "Input Files", "Countries.csv")) == true)
                            {
                                this.text_error.Text = "Error with the csv file. Please check the correct spelling or the folder location (PIDs.csv)";
                            };
                        }
                    }
                    this.text_folder.Text = fbd.SelectedPath;
                    this.text_PIDs.Text = _fileReadingGateway.PIDs_Dictionary.GetLength(0).ToString();
                }
            }
        }

        public void ProgressBar_PerformStep()
        {
            this.ProgressBar.PerformStep();
            this.ProgressBar_Percentage.Text = Convert.ToString(Math.Round(Convert.ToDouble(this.ProgressBar.Value) / Convert.ToDouble(this.ProgressBar.Maximum) * 100, 0)) + "%";
        }

        private void LittlePrinceForm_Load(object sender, EventArgs e)
        {

        }

        private void btn_refresh_Click(object sender, EventArgs e)
        {
            if (this.text_folder.Text.Equals(""))
            {
                this.text_error.Text = "Please click at the folder icon and select a path";
                return;
            }
            else
            {
                if (_fileReadingGateway.Read_InputPIDs(Path.Combine(this.text_folder.Text, "Input Files", "PIDs.csv"),Path.Combine(this.text_folder.Text, "Input Files", "Countries.csv")) == true)
                {
                    this.text_error.Text = "Error with the csv file. Please check the correct spelling or the folder location (PIDs.csv)";
                };
                this.text_PIDs.Text = _fileReadingGateway.PIDs_Dictionary.Length.ToString();
            }
        }
    }
}
