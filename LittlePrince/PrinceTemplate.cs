﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Excel = Microsoft.Office.Interop.Excel;

namespace LittlePrince
{

    public class PrinceTemplate
    {

        Excel.Application xl = new Excel.Application();

        public void FullPrice_Template(IDictionary<string[], OnlinePrice> dic_final_onlineprice, string princefullprice_template, string princefullprice_ready, string exchangerate_filename)
        {
            Excel.Workbook wb = xl.Workbooks.Open(princefullprice_template);
            Excel.Workbook wb_exchange = xl.Workbooks.Open(exchangerate_filename);

            int row = 5;
            double exchange_rate;
            string oldvariant = null;

            Excel.Range rg = (Excel.Range)wb.Worksheets[1].Range[wb.Worksheets[1].Cells[1, 1], wb.Worksheets[1].Cells[1, wb.Worksheets[1].Range("A1").End(Excel.XlDirection.xlToRight).Column]];
            Excel.Range rg_exchange = (Excel.Range)wb.Worksheets[1].Range[wb.Worksheets[1].Cells[1, 6], wb.Worksheets[1].Cells[wb.Worksheets[1].Range("F1").End(Excel.XlDirection.xlDown).Row, 6]];

            foreach (var variants in dic_final_onlineprice)
            {
                //try
                //{

                    int findrg = rg.Find(variants.Key[1]).Column;
                        
                        if(variants.Key[2] != variants.Key[3])
                        {
                            int row_exrate = rg_exchange.Find(String.Concat("FW18", variants.Key[2], variants.Key[3])).Row;
                            exchange_rate = wb.Worksheets[1].Range(String.Concat("F", row_exrate)).Value;
                        }
                        else {exchange_rate = 1;}

                        if (variants.Value.onSale == true)
                        {
                            wb.Worksheets[1].Cells[row, 1].Value = variants.Key[0];
                            wb.Worksheets[1].Cells[row,findrg].Value = (Convert.ToDouble(variants.Value.price.originalAmount) / Convert.ToDouble(variants.Value.price.divisor)) * exchange_rate;
                        }
                        else
                        {
                            wb.Worksheets[1].Cells[row, 1].Value = variants.Key[0];
                            wb.Worksheets[1].Cells[row, findrg].Value = Convert.ToDouble(variants.Value.price.amount) / Convert.ToDouble(variants.Value.price.divisor) * exchange_rate;
                        }

                        //_lpForm.ProgressBar_PerformStep();
                        if(String.IsNullOrEmpty(oldvariant) == true || oldvariant.Equals(variants.Key[0]) )
                        { }
                        else
                        {
                            row++;
                        }
                        oldvariant = variants.Key[0];
                        
                //}
                //catch
                //{
                    
                //}

            }

            wb.SaveAs(princefullprice_ready);
            wb.Close();
            wb_exchange.Close();
        }

        public void MarkDown_Template(IDictionary<string[], OnlinePrice> dic_final_onlineprice, string princemarkdown_template, string princemarkdown_ready)
        {
            Excel.Workbook wb = xl.Workbooks.Open(princemarkdown_template);
            int row = 3;
            string oldvariant = null;

            foreach (var variants in dic_final_onlineprice)
            {
                //try
                //{
                    Excel.Range rg = (Excel.Range)wb.Worksheets[1].Range[wb.Worksheets[1].Cells[1, 1], wb.Worksheets[1].Cells[1, wb.Worksheets[1].Range("A1").End(Excel.XlDirection.xlToRight).Column]];
                    int findrg = rg.Find(variants.Key[1]).Column;

                    if (variants.Value.onSale == true)
                    {
                        wb.Worksheets[1].Cells[row, 1].Value = variants.Key[0];
                        wb.Worksheets[1].Cells[row, findrg].Value = variants.Value.price.discountPercent;

                        if (String.IsNullOrEmpty(oldvariant) == true || oldvariant.Equals(variants.Key[0]))
                        { }
                        else{ row++; }
                        oldvariant = variants.Key[0];
                    }

                    //_lpForm.ProgressBar_PerformStep();

                //}
                //catch
                //{

                //}

            }

            wb.SaveAs(princemarkdown_ready);
            wb.Close();
        }

    }
}
