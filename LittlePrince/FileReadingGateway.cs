﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace LittlePrince
{
    public class FileReadingGateway
    {
        public string[,] PIDs_Dictionary;
        public bool exception = false;
        public int i = 0;

        public bool Read_InputPIDs(string pids_filename, string countries_filename)
        {
            var regex = new Regex("[,;]");
            PIDs_Dictionary = new string[(File.ReadLines(pids_filename).Count() - 1) *(File.ReadLines(countries_filename).Count() - 1), 2];
            //try
            //{
                using (var reader = new StreamReader(pids_filename))
                {
                    reader.ReadLine();
                    while (!reader.EndOfStream)
                    {
                        var pids_values = regex.Split(reader.ReadLine());
                        using (var reader_two = new StreamReader(countries_filename))
                        {
                            reader_two.ReadLine();

                            while(!reader_two.EndOfStream)
                            {
                                var country_values = regex.Split(reader_two.ReadLine());
                                PIDs_Dictionary[i,0] = pids_values[0];
                                PIDs_Dictionary[i,1] = country_values[0];
                                i++;
                            }
                        }

                        
                    }
                }
                return exception;
            //}
            //catch
            //{
                //exception = true;
                //return exception;
            //}

            
        }
    }
}
