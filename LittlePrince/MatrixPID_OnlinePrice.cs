﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace LittlePrince
{
    public class MatrixPID_OnlinePrice
    {

        public IDictionary<string[], OnlinePrice> dic_pidcountry_onlineprice = new Dictionary<string[], OnlinePrice>();
        

        public void ConstructMatrix(string PID, OnlinePrice onlinePrice)
        {
            var regex = new Regex("[,;]");
            dic_pidcountry_onlineprice.Add(regex.Split(PID), onlinePrice);
        }
    }
}
