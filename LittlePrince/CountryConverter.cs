﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LittlePrince
{
    class CountryConverter
    {

        public IDictionary<string[], OnlinePrice> dic_final_onlineprice = new Dictionary<string[], OnlinePrice>();
        private LittlePrinceForm _lpForm;
        public bool MappingFound;

        public void UpdateKey(IDictionary<string[], OnlinePrice> variants_dic, string error_filename, List<string[]> mappingCountry_Name)
        {

            _lpForm = new LittlePrinceForm();

            foreach (var i_key in variants_dic)
            {
                string country_FromDictionary = i_key.Key[1];
                MappingFound = false;

                for (int i = 0; i < mappingCountry_Name.Count; i++)
                {
                    if (mappingCountry_Name[i][1].Equals(country_FromDictionary))
                    {
                        dic_final_onlineprice.Add(new[] {i_key.Key[0],mappingCountry_Name[i][0],mappingCountry_Name[i][2],mappingCountry_Name[i][3] }, i_key.Value);
                        MappingFound = true;
                    }
                }

                if (MappingFound == false)
                {
                    _lpForm.WriteLine(String.Concat(country_FromDictionary, "COUNTRY_MAPPING_NOTFOUND"), error_filename);
                }

            }

        }

    }
}
